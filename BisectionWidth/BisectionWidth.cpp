// BisectionWidth.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <sstream>
#include <stdlib.h>
//#include <chrono>  
#include <omp.h>
#include <queue>   
#include <mpi.h>
#include <stdint.h>

/** TAG definitions **/
#define TAG_WORK 0
#define TAG_DONE 1
#define TAG_FINISHED 2

/** CONSTANTS definitions **/
#define EXPECTED_NUM_OF_THREADS 4
#define PARALLEL_COEF 10
#define EXPECTED_NUM_OF_PROCESSES 4
#define PROCESS_COEF 10
#define SEQUENTIAL_LIMIT 2

bool** edges=NULL;
int nodesCount;
int subsetSize;

bool* bestSolution;
int bestWidth;

using namespace std;



void loadData(const char * file) {
	ifstream input;
	input.open(file);
	if (!input.is_open())
	{
		throw new runtime_error("Unable to open file");
	}
	string data;
	stringstream ss;
	getline(input, data);
	if (data.size() < 1) throw new runtime_error("Loading data error");
	ss << data;
	ss >> nodesCount;
	edges = new bool*[nodesCount];
	for (int i = 0; i < nodesCount; i++){
		edges[i] = new bool[nodesCount];
		getline(input, data);
		//control output
		//cout << data <<data.size()<< endl;
		if(data.size()< nodesCount) throw new runtime_error("Loading data error");
		for (int a = 0; a < nodesCount; a++){
			edges[i][a] = ((data[a] == '1') ? true:false);
		}

	}
}

void testPrint() {
	for (int a = 0; a < nodesCount; a++)
	{
		for (int b = 0; b < nodesCount; b++)
		{
			cout<<edges[a][b];
		}
		cout << endl;
	}
}

void printSolution(bool* sol) {
	for (int i = 0; i < nodesCount; i++)
	{
		cout << sol[i];
	}
	cout << endl;
}

bool * copySolution(bool *exemplar) {
	bool* result = new bool[nodesCount];
	memcpy(result, exemplar, nodesCount*sizeof(bool));
	return result;
}

int subsetCount(bool* solution,int countSize) {
	int counter = 0;
	for (int i = 0; i < countSize; i++){
		counter += solution[i] ? 1 : 0;
	}
	return counter;
}

int widthCount(bool* solution) {
	int edgesCount = 0;
	for (int i = 0; i < nodesCount; i++){
		for (int a = i+1; a < nodesCount; a++){
			if (solution[i] != solution[a] && edges[i][a]) {
				edgesCount++;
			}
		}
	}
	return edgesCount;
}

void solveRecursively(bool* solution, int currentIndex,bool** bestSolution,int *bestWidth,int addedCount){
	//cout << "solveRecursively " << currentIndex << " " << addedCount << " " << *bestWidth << endl;
	//#pragma omp	critical
	//	cout << addedCount << " " <<currentIndex<<" "<< bestWidth << endl;
	if (currentIndex == nodesCount && addedCount==subsetSize) {
		//#pragma omp critical	
		//cout << "check" <<" "<<bestWidth<< endl;
		int width = widthCount(solution);
		if (width < *bestWidth) {
			#pragma omp critical  
			{
				if (width < *bestWidth) {
					(*bestWidth) = width;
					//cout <<*bestWidth<<" "<< *bestSolution <<" "<<bestWidth <<" "<<addedCount<<" "<<currentIndex<< endl;
					delete[] * bestSolution;
					(*bestSolution) = (copySolution(solution));
					//cout << *bestSolution << endl;
					//printSolution(solution);
				}
			}
		}
		return;
	}
	if (addedCount < subsetSize) {
		solution[currentIndex] = true;
		//most likely unnecessary condition
		if (currentIndex < nodesCount) solveRecursively(solution, currentIndex + 1, bestSolution, bestWidth, addedCount + 1);
	}
	//no else
	if (currentIndex - addedCount < nodesCount - subsetSize) {
		solution[currentIndex] = false;
		//most likely unnecessary condition
		if (currentIndex < nodesCount) solveRecursively(solution, currentIndex + 1, bestSolution, bestWidth, addedCount);
	}
	
}

void nextSolution(bool* solution, int depth) {
	//cout << "next Sol start:"; printSolution(solution);
	while (solution[depth] != false) {
		solution[depth--] = false;
	}
	solution[depth] = true;
	//cout << "next Sol end:"; printSolution(solution);
}




void computeParallelLocal(bool* task,int definedDepth,bool**bestSolution,int* bestWidth) {
	int definedCount = 0;
	
	//divide tasks
	int tasksCount = EXPECTED_NUM_OF_THREADS*PARALLEL_COEF;
	int depth = 0;
	while (tasksCount){
		depth++;
		tasksCount = tasksCount >> 1;
	}
	//mozny problem
	if (depth+definedDepth > nodesCount-SEQUENTIAL_LIMIT) {
		depth = nodesCount - SEQUENTIAL_LIMIT -definedDepth;
		//cout << "Too many tasks requested!!!!!" << endl;
	}
	tasksCount = 1 << depth;
	//cout << depth << " " << tasksCount << endl;
	bool** solutions = new bool*[tasksCount];
	solutions[0] = copySolution(task);
	if (subsetCount(solutions[0], definedDepth) > subsetSize) {
		(*bestSolution) = solutions[0];
		(*bestWidth) = nodesCount*nodesCount;
		delete[] solutions;
		return;
	}
	for (int i = definedDepth; i < nodesCount; i++) solutions[0][i] = false;
	for (int i = 1; i < tasksCount; i++)
	{
		//cout << i << " ";
		solutions[i] = copySolution(solutions[i - 1]);
		nextSolution(solutions[i],definedDepth+depth-1);
		//cout << "solving "; printSolution(solutions[i]); cout << " " << depth + definedDepth << endl;
	}
	
	#pragma omp parallel for schedule(static,1)
	for (int i = 0; i < tasksCount; i++)
	{
		int addedCount = subsetCount(solutions[i], depth+definedDepth);
		if (addedCount > subsetSize) {
			//cout << "SLAVE - jumped over" << endl;
			continue;
		}
		//cout << "solving "; printSolution(solutions[i]); cout << " " << depth+definedDepth << " " << addedCount<<endl;
		solveRecursively(solutions[i], depth+definedDepth, bestSolution, bestWidth, addedCount);
		delete[] solutions[i];
	}

	
	/*cout << bestWidth << " " << endl;
	for (int i = 0; i < nodesCount; i++){
		cout << bestSolution[i];
	}
	cout << endl;*/

	delete[] solutions;
	return;
}

#define EXPECTED_NUM_OF_PROCESSES 4
#define PROCESS_COEF 10

bool** prepareTasks(bool***tasks,int* count,int*defined) {
	//divide tasks
	int tasksCount = EXPECTED_NUM_OF_PROCESSES*PROCESS_COEF;
	int depth = 0;
	while (tasksCount) {
		depth++;
		tasksCount = tasksCount >> 1;
	}
	
	if (depth  > nodesCount - SEQUENTIAL_LIMIT) {
		depth = nodesCount - SEQUENTIAL_LIMIT;
		//cout << "Too many tasks requested!!!!!" << endl;
	}
	tasksCount = 1 << depth;
	//cout << depth << " " << tasksCount << endl;
	bool** tasksPool = new bool*[tasksCount];
	tasksPool[0] = new bool[nodesCount];
	for (int i = 0; i < nodesCount; i++) tasksPool[0][i] = false;
	for (int i = 1; i < tasksCount; i++)
	{
		//cout << i << " ";
		tasksPool[i] = copySolution(tasksPool[i - 1]);
		nextSolution(tasksPool[i], depth - 1);
	}
	(*tasks) = tasksPool;
	(*count) = tasksCount;
	(*defined) = depth;

}

uint64_t encodeSoultion(bool* solution) {
	uint64_t ret = 0;

	/*int my_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	cout <<my_rank<< " encoding solution "; printSolution(solution);
*/
	for (int i = 0; i < nodesCount; i++) {	
		if(solution[i])	
			ret |= ((uint64_t)1) << i;
	}
	//cout << my_rank << " " << ret << endl;
	return ret;

}

bool * decodeSolution(uint64_t data) {
	bool * solution = new bool[nodesCount];
	//int my_rank;
	//MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	//cout << my_rank << " decoding solution " << data << endl;

	for (int i = 0; i < nodesCount; i++) {
		if ((data>>i)& 1 ) solution[i] = true; 
		else solution[i] = false;
	}

	//cout << my_rank << " "; printSolution(solution);

	return solution;
}

#define LENGTH 100
//returns the adress where does the message comes from
int receiveAndEvaluate() {
	char buffer[LENGTH];
	int position = 0;
	int width;
	uint64_t data;
	MPI_Status status;
	MPI_Recv(buffer, LENGTH, MPI_PACKED, MPI_ANY_SOURCE, TAG_DONE, MPI_COMM_WORLD, &status);
	MPI_Unpack(buffer, LENGTH, &position, &width, 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(buffer, LENGTH, &position, &data, 1, MPI_UINT64_T, MPI_COMM_WORLD);
	bool * solution = decodeSolution(data);
	/*cout << "MASTER - received TAG_DONE message from " << status.MPI_SOURCE <<" with width "<< width<<" "<< endl;
	cout << "Master -"; printSolution(solution);*/
	if (width < bestWidth) {
		bestWidth = width;
		delete[] bestSolution;
		bestSolution = solution;
	}
	else delete[] solution;
	return status.MPI_SOURCE;
}


void sendTask(bool* task,int const depth,int dest) {
	int position = 0;
	char buffer[LENGTH];
	uint64_t data = encodeSoultion(task);
	MPI_Pack(&depth, 1, MPI_INT, buffer, LENGTH, &position, MPI_COMM_WORLD);
	MPI_Pack(&data, 1, MPI_UINT64_T, buffer, LENGTH, &position, MPI_COMM_WORLD);
	MPI_Send(buffer, position, MPI_PACKED, dest, TAG_WORK, MPI_COMM_WORLD);
}

void sendFinished(int dest) {
	int buffer;
	MPI_Send(&buffer, 0, MPI_INT, dest, TAG_FINISHED, MPI_COMM_WORLD);
}

int receiveMessagesAndDistributeTasks(bool**tasks,int tasksCount,const int depth,int* nextTask, int* doneCount) {
	MPI_Status status; int flag;
	//cout << "MASTER - Receiving and distribution started" << endl;
	MPI_Iprobe(MPI_ANY_SOURCE, TAG_DONE, MPI_COMM_WORLD, &flag, &status);
	while (flag) {
		if (flag) {
			//receive solution
			int destination = receiveAndEvaluate();
			(*doneCount)++;
			//assign work if there is any
			//move to slave
			//while ((*nextTask) < tasksCount && subsetCount(tasks[(*nextTask)], depth) > subsetSize) {
			//	(*nextTask)++;
			//	(*doneCount)++;
			//	//cout << "MASTER - jumped over" << endl;
			//}
			if ((*nextTask) < tasksCount) {
				sendTask(tasks[(*nextTask)], depth,destination);
				(*nextTask)++;
			}
			else {
				sendFinished(destination);
			}
		}
		MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
	}
	//cout << "MASTER - Receiving and distribution done, remains"<< tasksCount - (*nextTask) <<" tasks"<< endl;
	return tasksCount - (*nextTask);
}

void receiveAndFinish(int tasksCount,int doneCount) {
	while (doneCount<tasksCount)
	{
		int destination = receiveAndEvaluate();
		doneCount++;
		sendFinished(destination);
	}

}

void initialDistribution(bool**tasks, int tasksCount, const int depth, int* nextTask) {
	int p;
	//cout << "MASTER - Initial distribution started" << endl;
	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	for (int i = 1; i < p; i++) {
		if (*nextTask == tasksCount) {
			sendFinished(i);
		}
		else {
			sendTask(tasks[(*nextTask)], depth, i);
			(*nextTask)++;
		}
	}
	//cout << "MASTER - Initial distribution finished" << endl;
}
#define MASTER_RANK 0 
void sendSlaveResult(int width, bool* sol) {
	int position = 0;
	char buffer[LENGTH];
	uint64_t data = encodeSoultion(sol);
	MPI_Pack(&width, 1, MPI_INT, buffer, LENGTH, &position, MPI_COMM_WORLD);
	MPI_Pack(&data, 1, MPI_UINT64_T, buffer, LENGTH, &position, MPI_COMM_WORLD);
	MPI_Send(buffer, position, MPI_PACKED, MASTER_RANK, TAG_DONE, MPI_COMM_WORLD);
}

void slaveJob() {
	char buffer[LENGTH];
	int position;
	int depth;
	uint64_t data;
	MPI_Status status;
	int flag;
	while (true) {
		MPI_Iprobe(MASTER_RANK, TAG_WORK, MPI_COMM_WORLD, &flag, &status);
		if (flag) {
			MPI_Recv(buffer, LENGTH, MPI_PACKED, MASTER_RANK, TAG_WORK, MPI_COMM_WORLD, &status);
			position = 0;
			MPI_Unpack(buffer, LENGTH, &position, &depth, 1, MPI_INT, MPI_COMM_WORLD);
			MPI_Unpack(buffer, LENGTH, &position, &data, 1, MPI_UINT64_T, MPI_COMM_WORLD);
			bool* task = decodeSolution(data);
			//test print
			/*int my_rank;
			MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
			cout << my_rank<< " received task with depth " << depth << endl;
			cout << my_rank << " "; printSolution(task);*/
			//solve
			bool* bestSolution = new bool[nodesCount];
			int bestWidth = nodesCount*nodesCount;
			computeParallelLocal(task,depth,&bestSolution,&bestWidth);
			//reply
			/*cout << my_rank << " Sending solution with width " << bestWidth << endl;
			cout << my_rank << " "; printSolution(bestSolution);*/
			sendSlaveResult(bestWidth,bestSolution);
			delete[] task;
			delete[] bestSolution;
		}
		else {
			MPI_Iprobe(MASTER_RANK, TAG_FINISHED, MPI_COMM_WORLD, &flag, &status);
			if (flag) {
				MPI_Recv(buffer, LENGTH, MPI_INT, MASTER_RANK, TAG_FINISHED, MPI_COMM_WORLD, &status);
				/*int my_rank;
				MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
				cout << my_rank << " TAG_DONE received  - going to die" << endl;*/
				break;
			}
		}
		
	}
	
	
}

int main(int argc, char *argv[])
{
	

	if (argc != 3) {
		cout << "Wrong params"<<argc;
		cout << argv[1] << argv[2];//<< argv[2] << endl;
		throw new runtime_error("Wrong params"+argc);
		return 1;
	}
	loadData(argv[2]);
	subsetSize = atoi(argv[1]);
	//because of usage of uint64
	if (nodesCount > 64) {
		throw new runtime_error("Too many nodes");
	}
	//TODO check if subsetsize<nodesCount-subsetSize
	//cout << nodesCount << " " << subsetSize<<endl;
	//testPrint();

	int my_rank;
	int p;
	/* start up MPI - move up */
	MPI_Init(&argc, &argv);

	/* find out process rank */
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	/* find out number of processes */
	MPI_Comm_size(MPI_COMM_WORLD, &p);


	if (my_rank == MASTER_RANK) {
		//master thread job
		double time = MPI_Wtime();
		bestSolution = new bool[nodesCount];
		bestWidth = nodesCount*nodesCount;
		bool ** tasksPool;
		int tasksCount, depth ;
		int nextTask = 0;
		int doneCount = 0;
		prepareTasks(&tasksPool,&tasksCount,&depth);
		initialDistribution(tasksPool, tasksCount, depth, &nextTask);
		while (receiveMessagesAndDistributeTasks(tasksPool,tasksCount,depth,&nextTask,&doneCount)>0) {
			//TODO work
			//if ((nextTask) < tasksCount) {
			if(false){
				bool* solution = new bool[nodesCount];
				int width = nodesCount*nodesCount;
				computeParallelLocal(tasksPool[nextTask], depth, &solution, &width);
				if (width < bestWidth) {
					bestWidth = width;
					delete[] bestSolution;
					bestSolution = solution;
				}
				else delete[] solution;
				doneCount++;
				nextTask++;
			}
		}
		receiveAndFinish(tasksCount,doneCount);
		//print solution                                                                                                                                                                                                                                                                                                                                                                                                                          
		cout << bestWidth << endl;
		printSolution(bestSolution);
		cout << MPI_Wtime() - time << endl;
	}
	else {
		//slave thread job
		slaveJob();
	}

	
	//chrono::steady_clock::time_point start = chrono::steady_clock::now();
	
	

	//chrono::steady_clock::time_point end = chrono::steady_clock::now();
	
	//cout << "Time:" << chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
	//	<< "ms." << endl;
	
	for (int i = 0; i < nodesCount; i++) {
		delete[] edges[i];
	}
	delete[] edges;
	
	MPI_Finalize();
	return 0;
}

